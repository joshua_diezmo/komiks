import cheerio from 'cheerio-without-node-native';
const moment = require('moment');
module.exports = {
    logEnable: false,
    unEntity: function (str) {
        str = str.split('&amp;').join('&');
        str = str.split('&lt;').join('<');
        str = str.split('&gt;').join('>');
        str = str.split('&apos;').join("'");
        console.log(str);
        return str;
    },
    log: function (func, params) {
        if (this.logEnable) {
            if (params)
                console.log('website-crawler', func, params);
            else
                console.log('website-crawler', func);
        }
    },
    load: async function (url, regex) {
        try {
            this.log('load', {url, regex});
            let response = await fetch(url);

            let count = 0, max = 20;
            let promise = new Promise((resolve, reject) => {
                let interval = setInterval(async() => {
                    if (count < max) {
                        let content = await response.text();

                        let items = [];
                        var re = regex;
                        var s = content;
                        var m;

                        do {
                            m = re.exec(s);
                            if (m) {
                                items.push(m[1]);
                            }
                        } while (m);
                        if (items.length > 0) {
                            clearInterval(interval);
                            resolve(items);
                        }
                        else
                            count++;
                    } else {
                        clearInterval(interval);
                        reject('Timeout in load function');
                    }
                }, 1000);
            });
            let elements = await promise;
            return elements;
        } catch (err) {
            alert(err.message)
            throw (err);
        }
    },
    getElements: async function (url, selector) {
        try {
            this.log('getElements', {url, selector});
            let response = await fetch(url);

            if (!selector)
                return status;
            else {
                let count = 0, max = 20;
                let promise = new Promise((resolve, reject) => {
                    let interval = setInterval(async() => {
                        try {
                            if (count < max) {
                                let content = await response.text();

                                const $ = cheerio.load(content);
                                let elements = $(selector);
                                if (elements.length > 0) {
                                    clearInterval(interval);
                                    resolve(elements);
                                }
                                count++;
                            } else {
                                clearInterval(interval);
                                reject('Timeout in getElements function');
                            }
                        } catch (err) {
                            throw Error(err);
                        }
                    }, 1000);
                });
                let elements = await promise;
                return elements;
            }
        } catch (err) {
            alert(err.message)
            throw (err);
        }
    },
    getIssues: function (id) {
        this.log('getIssues', {id});
        let self = this;
        return new Promise(async(resolve, reject) => {
            try {
                let elements = await self.getElements('https://readcomics.io/comic/' + id, '.wbg-content');
                let list = elements.eq(0).find('.basic-list').find('li');
                let issues = [];
                for (let x = 0; x < list.length; x++) {
                    let url, issueTitle, dateRelease;
                    let item = list.eq(x);
                    let aTag = item.find('a').eq(0);

                    url = aTag.attr('href');
                    issueTitle = aTag.html();
                    dateRelease = item.find('span').html();

                    if (url && issueTitle && dateRelease) {
                        url = url.trim();
                        issueTitle = issueTitle.trim();
                        issueTitle = self.unEntity(issueTitle || '');
                        dateRelease = dateRelease.trim();
                        dateRelease = moment(dateRelease, 'M/DD/YYYY').toDate();
                        issues.push({url, issueTitle, dateRelease});
                    }
                }
                resolve(issues);
            } catch (err) {
                reject(err);
            }
        })
    },
    getList: function () {
        let self = this;
        return new Promise(async(resolve, reject) => {
            try {
                let elements = await this.getElements('https://readcomics.io/comic-list', 'body');
                let listElements = elements.find('.serie-box');
                let popularElements = elements.find('.elliptic-list.lineList');
                let comics = [];
                for (let x = 0; x < listElements.length; x++) {
                    let group = listElements.eq(x).attr('id');
                    let listElement = listElements.eq(x).find('li');
                    let list = [];
                    for (let y = 0; y < listElement.length; y++) {
                        let item = listElement.eq(y);
                        let aTag = item.find('a').eq(0);
                        let title = aTag.html();
                        title = self.unEntity(title || '');
                        let url = aTag.attr('href');
                        let urlSplit = url.split('/');
                        let id = urlSplit[urlSplit.length - 1];
                        list.push({title, id});
                    }
                    comics.push({group, list});
                }
                let ellipticBoxs = popularElements.find('.elliptic-box');
                let popularComics = [];
                for (let x = 0; x < ellipticBoxs.length; x++) {
                    let item = ellipticBoxs.eq(x);
                    let aTag = item.find('.eb-image').eq(0);
                    let url = aTag.attr('href');
                    let urlSplit = url.split('/');
                    let id = urlSplit[urlSplit.length - 1];
                    let imageTag = aTag.find('img').eq(0);
                    let imageUrl = imageTag.attr('src');
                    let title = item.find('.eb-right').eq(0).find('.big-link').eq(0).html();
                    title = self.unEntity(title || '');
                    popularComics.push({title, id, imageUrl});
                }
                resolve({popularComics, comics});
            } catch (err) {
                reject(err);
            }
        });
    }
};