var SQLite = require('react-native-sqlite-storage');
var db = SQLite.openDatabase("komiks.db", "1.0", "Komiks Database", 200000, openCB, errorCB);

function errorCB(err) {
    console.log("SQL Error: " + err);
}

function successCB() {
    console.log("SQL executed fine");
}

function openCB() {
    console.log("Database OPENED");
}

module.exports = {
    createTable: function () {
        db.transaction((tx) => {
            tx.executeSql('CREATE TABLE IF NOT EXISTS comics (`id` text primary key, `group` text,`title` text)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS popular_comics (`id` text primary key, `image_url` text,`title` text)');
        });
    },
    saveList: async function (comicsList) {
        try {
            let {popularComics, comics} = comicsList;

            let comicsItems = [];
            comics.map(comic => {
                let list = comic.list;
                list.map(item => {
                    comicsItems.push({group: comic.group, ...item});
                });
            });
            this.createTable();
            db.transaction((tx) => {
                comicsItems.map(item => tx.executeSql("INSERT OR REPLACE  INTO comics (`id` ,`group`,`title`) VALUES (`?`,`?`,`?`);", [item.id, item.group, item.title]));
                popularComics.map(item => tx.executeSql("INSERT OR REPLACE  INTO popular_comics (`id` ,`image_url`,`title`) VALUES (`?`,`?`,`?`);", [item.id, item.imageUrl, item.title]));
            });
        } catch (err) {
            throw err;
        }
    },
    getList: async function () {
        try {
            this.createTable();
            let comics = await this.getComicsList();
            let popularComics = await this.getPopularComicsList();
            return {popularComics, comics};
        } catch (err) {
            throw err;
        }
    },
    getComicsList: function () {
        let comics = [];
        let comicsTemp = {};
        return new Promise((resolve, reject) => {
            db.transaction((tx) => {
                tx.executeSql('SELECT * FROM comics', [], function (tx, res) {
                    for (let x = 0; x < res.rows.length; x++) {
                        let item = res.rows.item(x);
                        let {group, title, id} = item;
                        let list = comicsTemp[group] || [];
                        list.push({title, id});
                        comicsTemp[group] = list;
                    }
                    for (let key in comicsTemp) {
                        let list = comicsTemp[key];
                        comics.push({group: key, list})
                    }
                    resolve(comics);
                }, function (e) {
                    reject(e)
                });
            });
        })
    },
    getPopularComicsList: function () {
        let popularComics = [];
        return new Promise((resolve, reject) => {
            db.transaction((tx) => {
                tx.executeSql('SELECT * FROM popular_comics', [], function (tx, res) {
                    for (let x = 0; x < res.rows.length; x++) {
                        let item = res.rows.item(x);
                        item.imageUrl = item.image_url;
                        popularComics.push(item);
                    }
                    resolve(popularComics);
                }, function (e) {
                    reject(e)
                });
            });
        })
    }
};