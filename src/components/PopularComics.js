//Randolf Joshua Diezmo

import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {View, Text, FlatList, Image, TouchableNativeFeedback} from 'react-native';

class ListHeader extends Component {
    renderItem = ({item}) => {
        return (
            <TouchableNativeFeedback>
                <View style={{padding:5}}>
                    <Image
                        style={{width: 150, height: 210}}
                        source={{uri: item.imageUrl}}
                    />
                    <View
                        style={{position:'absolute',bottom:5,left:5,right:5,alignItems:'center',backgroundColor:'rgba(0,0,0,.5)',padding:5}}>
                        <Text style={{color:'#fff'}}>{item.title}</Text>
                    </View>
                </View>
            </TouchableNativeFeedback>
        )
    };

    render() {
        let popularComics = this.props.comicsList.list.popularComics;
        return (
            <View>
                <Text style={{padding:12,backgroundColor:'#ccc'}}>Popular Comics</Text>
                <FlatList
                    data={popularComics}
                    keyExtractor={item=>`propular-${popularComics.id}`}
                    horizontal
                    renderItem={this.renderItem}
                />
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const {comicsList} = state;
    const props = {comicsList};
    return props;
};

const mapDispatchToProps = (dispatch) => {
    const actions = {};
    const actionMap = {
        actions: bindActionCreators(actions, dispatch)
    };
    return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(ListHeader);