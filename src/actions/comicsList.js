import {COMICS_LIST_FETCHING, COMICS_LIST_FETCHED, COMICS_LIST_ERROR} from './const';
const websiteCrawler = require('website-crawler');
const comicsListSqlite = require('../sqlite/comicsList');
websiteCrawler.logEnable = true;

function action() {
    return async(dispatch) => {
        try {
            dispatch({type: COMICS_LIST_FETCHING});

            let list = await comicsListSqlite.getList();
            dispatch({type: COMICS_LIST_FETCHED, list});

            list = await websiteCrawler.getList();
            await comicsListSqlite.saveList(list);
            dispatch({type: COMICS_LIST_FETCHED, list});
        } catch (error) {
            dispatch({type: COMICS_LIST_ERROR, error});
        }
    }
}

module.exports = action;