import {ISSUES_LIST_FETCHING, ISSUES_LIST_FETCHED, ISSUES_LIST_ERROR} from './const';
const webCrawler = require('website-crawler');

function action(comics) {
    return async(dispatch) => {
        try {
            dispatch({type: ISSUES_LIST_FETCHING});
            let list = await webCrawler.getIssues(comics.id);
            dispatch({type: ISSUES_LIST_FETCHED, list, comics});
        } catch (error) {
            dispatch({type: ISSUES_LIST_ERROR, error});
        }
    }
}

module.exports = action;