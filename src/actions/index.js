import test from '../actions/test.js';
import comicsList from '../actions/comicsList.js';
import issuesList from '../actions/issuesList.js';

const actions = {
    test,
    comicsList,
    issuesList
};
module.exports = actions;