import {combineReducers} from 'redux';
import test from '../reducers/test.js';
import comicsList from '../reducers/comicsList.js';
import issuesList from '../reducers/issuesList.js';

const reducers = {
    test,
    comicsList,
    issuesList
};
const combined = combineReducers(reducers);
module.exports = combined;