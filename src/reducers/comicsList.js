import {COMICS_LIST_FETCHING, COMICS_LIST_FETCHED, COMICS_LIST_ERROR} from '../actions/const';

const initialState = {loading: false, list: {popularComics: [], comics: []}, error: null};

function reducer(state = initialState, action) {
    const nextState = Object.assign({}, state);

    switch (action.type) {

        case COMICS_LIST_FETCHING: {
            return {...nextState, loading: true, error: null};
        }

        case COMICS_LIST_FETCHED: {
            return {...nextState, loading: false, list: action.list, error: null};
        }

        case COMICS_LIST_ERROR: {
            return {...nextState, loading: false, error: action.error};
        }

        default: {
            /* Return original state if no actions were consumed. */
            return state;
        }
    }
}

module.exports = reducer;