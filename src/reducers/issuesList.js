import {ISSUES_LIST_FETCHING, ISSUES_LIST_FETCHED, ISSUES_LIST_ERROR} from '../actions/const';

const initialState = {loading: false, list: {}, error: null};

function reducer(state = initialState, action) {
    const nextState = Object.assign({}, state);

    switch (action.type) {

        case ISSUES_LIST_FETCHING: {
            return {...nextState, loading: true};
        }

        case ISSUES_LIST_FETCHED: {
            let max = 5, count = 0;
            let list = {};
            let temp = {...nextState.list, [action.comics.id]: {issues: action.list, ...action.comics}};
            let keys = [];
            for (let key in temp) {
                keys.push(key);
            }
            for (let x = keys.length - 1; x >= 0; x--) {
                let key = keys[x];
                if (count < max) {
                    list[key] = temp[key];
                    count++;
                }
            }
            return {...nextState, loading: false, list, error: null};
        }

        case ISSUES_LIST_ERROR: {
            return {...nextState, loading: false, error: action.error};
        }

        default: {
            /* Return original state if no actions were consumed. */
            return state;
        }
    }
}

module.exports = reducer;