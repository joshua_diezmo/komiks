//Randolf Joshua Diezmo

import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {issuesList} from '../actions';
import {View, Text, FlatList, StyleSheet} from 'react-native';

class IssuesList extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            title: navigation.getParam('title', 'Issues'),
        };
    };

    componentDidMount() {
        try {
            let item = this.props.navigation.state.params.item;
            let title = item.title;
            this.props.navigation.setParams({title});
            this.props.actions.issuesList(item)
        } catch (err) {
        }
    }

    getIssueList = () => {
        let list = [];
        try {
            let item = this.props.navigation.state.params.item;
            let issuesList = this.props.issuesList;
            list = issuesList.list[item.id].issues;
        } catch (err) {

        }
        return list;
    };

    render() {
        let issuesList = this.getIssueList();
        return (
            <View>
                <FlatList
                    data={issuesList}
                    keyExtractor={item=>item.issueTitle}
                    ItemSeparatorComponent={()=><View style={{borderBottomWidth:StyleSheet.hairlineWidth,borderColor:'#aaa'}}/>}
                    renderItem={({item}) => <Text style={{fontSize:16,padding:20}}>{item.issueTitle}</Text>}
                />
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const {issuesList} = state;
    const props = {issuesList};
    return props;
};

const mapDispatchToProps = (dispatch) => {
    const actions = {issuesList};
    const actionMap = {
        actions: bindActionCreators(actions, dispatch)
    };
    return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(IssuesList);