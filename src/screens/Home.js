//Randolf Joshua Diezmo

import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {comicsList} from '../actions';
import {View, Text, FlatList, StyleSheet, TouchableNativeFeedback} from 'react-native';
import PopularComics from '../components/PopularComics';

class Home extends Component {
    static navigationOptions = {
        title: 'Komiks',
    };

    state = {
        listViewData: []
    };

    componentDidMount() {
        this.props.actions.comicsList();
    }

    renderItem = ({item}) => {
        return (
            <TouchableNativeFeedback onPress={()=>{
                this.props.navigation.push('IssuesList',{item});
            }}>
                <View>
                    <Text style={{padding:20,paddingLeft:15,fontSize:16}}>{item.title}</Text>
                </View>
            </TouchableNativeFeedback>
        )
    };

    pushList = () => {
        let page = this.state.page || 0;
        page += 1;
        let maxItems = page * 40, count = 0;
        let comicsList = this.props.comicsList.list.comics;

        let listViewData = [];
        for (let x = 0; x < comicsList.length; x++) {
            let item = comicsList[x];
            let list = item.list || [];
            list.map(i => {
                if (count < maxItems) {
                    listViewData.push(i);
                    count++;
                }
            });
        }
        return listViewData;
    };

    render() {
        return (
            <View>
                <FlatList
                    ListHeaderComponent={<PopularComics/>}
                    data={this.pushList()}
                    extraData={this.state.page}
                    keyExtractor={item=>item.id}
                    ItemSeparatorComponent={()=><View style={{borderBottomWidth:StyleSheet.hairlineWidth,borderColor:'#aaa'}}/>}
                    renderItem={this.renderItem}
                    onEndReached={distance=>{
                        console.log(distance)
                        this.setState({page:(this.state.page||0)+1})
                    }}
                />
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const {comicsList} = state;
    const props = {comicsList};
    return props;
};

const mapDispatchToProps = (dispatch) => {
    const actions = {comicsList};
    const actionMap = {
        actions: bindActionCreators(actions, dispatch)
    };
    return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);