//Randolf Joshua Diezmo
import React, {Component} from 'react';
import store from './src/store';
import {Provider} from 'react-redux';
import {StatusBar, View} from 'react-native';
import {createStackNavigator} from 'react-navigation';
import {primaryDark, primary} from './src/colors';
import Home from './src/screens/Home';
import IssuesList from './src/screens/IssuesList';

const MainApp = createStackNavigator({
    Home, IssuesList
}, {
    navigationOptions: {
        headerStyle: {
            elevation: 1,
            backgroundColor: primary
        },
        headerTintColor: '#fff'
    }
});

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <View style={{flex:1}}>
                    <StatusBar backgroundColor={primaryDark}/>
                    <MainApp/>
                </View>
            </Provider>
        );
    }
}

export default App;